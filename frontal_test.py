import sys
import pandas as pd
import os.path
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.alert import Alert
import time

#pip3 install pandas (use python 3.9 for better support of pandas!)
#pip3 install openpyxl
#pip3 install selenium

#Selenium drivers
#   Windows
#       chrome  :   https://sites.google.com/a/chromium.org/chromedriver/downloads
#       firefox :   https://github.com/mozilla/geckodriver/releases/download/v0.29.0/geckodriver-v0.29.0-win64.zip
#       edge    :   https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/#downloads
#
#       locate  chromedriver.exe
#               geckodriver.exe
#               MicrosoftWebDriver.exe (THIS ONE MUST BE RENAMED FROM msedgedriver.exe)
#       in %PATH% (such as C:\Windows\System32)
#
#   Ubuntu
#       ??? TODO!

#-----------------------------------------------------------------------------------------------------------------------
def confirm_unit_test(current_step):
    print('   Expected result is ' + current_step['expected_result'])
    human_verification = None
    while (human_verification == None):
        human_verification = input('Are you ok? Y/N').upper()
        if (human_verification not in ['Y', 'N']):    human_verification = None
    return human_verification
#-----------------------------------------------------------------------------------------------------------------------
def run_unit_test(driver,test_to_perform,tests_list):
    print('RUNNING UNIT TEST id='+str(test_to_perform['test_id'])+' desc='+test_to_perform['test_desc'])
    for current_step in test_to_perform['steps']:
        print('    '+str(current_step))
        if(current_step['prerequisite']!=None):
            referenced_prerequisite =   [x for x in tests_list if x['test_id']==current_step['prerequisite']][0]
            run_unit_test(driver,referenced_prerequisite,tests_list)
            if(current_step['expected_result']!=None):
                verification = confirm_unit_test(current_step)
                if(verification == 'N'):
                    print('FAILED TEST: id=' + str(test_to_perform['test_id']) + ' desc=' + test_to_perform['test_desc'],file=sys.stderr)
        else:
            there_are_erros =   False
            if(current_step['url']!=None):
                try:
                    driver.get(current_step['url'])
                except Exception as e:
                    there_are_erros =   True
                    print('    INVALID URL: '+current_step['url'],file=sys.stderr)
            if(there_are_erros==True):
                print('FAILED TEST: id=' + str(test_to_perform['test_id']) + ' desc=' + test_to_perform['test_desc'],file=sys.stderr)
            else:
                element_to_interact_with    =   driver.find_element_by_id(current_step['html_id'])
                if(element_to_interact_with==None):
                    there_are_erros =   True
                if(there_are_erros==True):
                    print('    NOT FOUND html_id=' + current_step['html_id'], file=sys.stderr)
                    print('FAILED TEST: id=' + str(test_to_perform['test_id']) + ' desc=' + test_to_perform['test_desc'],file=sys.stderr)
                if(there_are_erros==False):
                    if(  current_step['action']=='fill'):
                        element_to_interact_with.send_keys(current_step['value'])
                    elif(current_step['action']=='file_upload'):
                        element_to_interact_with.send_keys(current_step['value'])
                    elif(current_step['action']=='click'):
                        element_to_interact_with.click()
                    elif(current_step['action']=='select'):
                        select  =   Select(driver.find_element_by_id(current_step['html_id']))
                        select.select_by_visible_text(current_step['value'])
                    time.sleep(current_step['sleep_msecs_after'] / 1000)
                    if(current_step['expected_result']!=None):
                        verification  =   confirm_unit_test(current_step)
                        if (verification == 'N'):
                            print('FAILED TEST: id=' + str(test_to_perform['test_id']) + ' desc=' + test_to_perform['test_desc'],file=sys.stderr)

#-----------------------------------------------------------------------------------------------------------------------
def main():
    #1 parse args
    if(len(sys.argv)<3 or 3<len(sys.argv)):
        print('WRONG NUMBER OF ARGS: frontal_test.py <chrome/firefox/edge> <spec_excel_file.xlsx>. ABORTING...')
        sys.exit(1)
    chosen_browser  =   sys.argv[1]
    excel_path      =   sys.argv[2]
    if(not os.path.exists(excel_path)):
        print('SPECIFIED EXCEL FILE '+excel_path+' DOES NOT EXIST! ABORTING...')
        sys.exit(2)
    pd_df       =   None
    try:
        pd_df   =   pd.read_excel(excel_path, sheet_name='TESTS')
    except ValueError as e:
        print(e)
        print('EXCEL SHEET "TESTS" NOT FOUND. ABORTING...')
        sys.exit(3)
    if(chosen_browser not in ['chrome','firefox','edge']):
        print('SUPPORTED BROWSERS (sys.argv[1]) are chrome,firefox,edge. Specified: '+chosen_browser)
        sys.exit(2)
    #2 parse input excel file
    tests_to_perform    =   []
    fsm_state           =   0
    current_test        =   None
    for current_index,current_row in pd_df.iterrows():
        print('parsing excel row '+str(current_index+2))
        if(  current_row['A']=='id'):
            fsm_state           =   1
            if(current_test!=None):
                tests_to_perform.append(current_test)
                current_test    =   None
        elif(fsm_state==1):
            test_id             =   int(current_row['A'])
            test_desc           =   current_row['B']
            current_test        =   {'test_id':test_id,'test_desc':test_desc,'steps':[]}
            fsm_state           =   2
        elif(fsm_state==2):
            #ignore step headers and go to state 3
            fsm_state           =   3
        elif(fsm_state==3):
            step                =   int(current_row['B'])
            prerequisite        =   current_row['C']
            url                 =   current_row['D']
            html_id             =   current_row['E']
            label               =   current_row['F']
            value               =   current_row['G']
            action              =   current_row['H']
            expected_result     =   current_row['I']
            sleep_msecs_after   =   current_row['J']
            current_test['steps'].append(   {
                                                'step'              :   step,
                                                'prerequisite'      :   prerequisite,
                                                'url'               :   url,
                                                'html_id'           :   html_id,
                                                'label'             :   label,
                                                'value'             :   value,
                                                'action'            :   action,
                                                'expected_result'   :   expected_result,
                                                'sleep_msecs_after' :   sleep_msecs_after
                                               }
                                        )
    if(current_test!=None):
        tests_to_perform.append(current_test)

    #3 syntactic and semantic analysis
    for current_test in tests_to_perform:
        print('SYNTACTIC AND SEMANTIC ANALYSIS FOR '+str(current_test['test_id'])+' '+current_test['test_desc'])
        for current_step in current_test['steps']:
            if(pd.isna(current_step['prerequisite'])):
                current_step['prerequisite']        =   None
            else:
                referenced_test_id  =   [x for x in tests_to_perform if x['test_id']==current_step['prerequisite']]
                if(referenced_test_id==[]):
                    print('SEMANTIC EXCEPTION: referenced prerequisite id='+str(current_step['prerequisite'])+' not found')
                    sys.exit(4)
            if(  pd.isna(current_step['url'])):
                current_step['url']                 =   None
            if(  pd.isna(current_step['html_id'])):
                current_step['html_id']             =   None
            if(  pd.isna(current_step['label'])):
                current_step['label']               =   None
            if(  pd.isna(current_step['value'])):
                current_step['value']               =   None
            if(  pd.isna(current_step['action'])):
                current_step['action']              =   None
            elif(current_step['action'] not in ['fill','click','select','file_upload']):
                print('SEMANTIC EXCEPTION: action='+current_step['action']+' not in [fill,click,select,file_upload]')
                sys.exit(4)
            if(  pd.isna(current_step['expected_result'])):
                current_step['expected_result']     =   None
            elif(current_step['expected_result'] not in ['success','fail']):
                print('SEMANTIC EXCEPTION: action='+current_step['expected_result']+' not in [success,fail]')
                sys.exit(4)
            if(  pd.isna(current_step['sleep_msecs_after'])):
                current_step['sleep_msecs_after']   = None
            else:
                current_step['sleep_msecs_after']   = int(current_step['sleep_msecs_after'])
    tests_ids   =   [x['test_id'] for x in tests_to_perform]
    if(len(tests_ids) != len(set(tests_ids))):
        print('SEMANTIC EXCEPTION: TESTS CONTAINS DUPLICATES BY test_id. ABORTING...')
        sys.exit(4)

    #4 sort tests by its order
    tests_to_perform = sorted(tests_to_perform, key=lambda k: k['test_id'])

    #5 run tests
    driver  =   None
    if(  chosen_browser=='chrome'):
        try:
            driver  =   webdriver.Chrome()
        except:
            print('ERROR WHEN CREATING webdriver.Chrome()')
            print("Please, verify you have installed: Chrome and that chromedriver.exe's according version is in %PATH%")
            sys.exit(5)
    elif(chosen_browser=='firefox'):
        try:
            driver  =   webdriver.Firefox()
        except:
            print('ERROR WHEN CREATING webdriver.Firefox()')
            print("Please, verify you have installed: firefox and that geckodriver.exe's is in %PATH%")
            sys.exit(5)
    elif(chosen_browser=='edge'):
        try:
            driver  =   webdriver.Edge()
        except:
            print('ERROR WHEN CREATING webdriver.Edge()')
            print("Please, verify you have installed: edge and that MicrosoftWebDriver.exe's is in %PATH%")
            sys.exit(5)




    while(True):
        print('----------------------------------------------------------')
        print('----------------------------------------------------------')
        print('----------------------------------------------------------')
        print('-2: Exit')
        print('-1: Run all tests in its test_id order')
        for current_test in tests_to_perform:
            print(str(current_test['test_id'])+': '+current_test['test_desc'])
        chosen_option   =   None
        try:
            chosen_option   =   int(input('Type your option:'))
        except ValueError:
            print("Not a valid number")
        if(chosen_option!=None):
            #check if typed option is valid
            if(chosen_option==-2):
                print('GOODBYE!')
                driver.quit()
                sys.exit(0)
            if(chosen_option==-1):
                print('RUNNING ALL TESTS')
                for current_test in tests_to_perform:
                    run_unit_test(driver,current_test,tests_to_perform)
            else:
                referenced_test_id  =   [x for x in tests_to_perform if x['test_id']==chosen_option]
                if(referenced_test_id==[]):
                    print("Not a valid number")
                else:
                    run_unit_test(driver,referenced_test_id[0],tests_to_perform)
#-----------------------------------------------------------------------------------------------------------------------
if(__name__=='__main__'):
    main()
